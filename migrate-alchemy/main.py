# -*- coding: utf-8 -*-

###############################################################################
# main.py
#
# author: Paweł Surowiec (codedigger)
# creation date: 04.12.2014
# version: 0.0.1
#
# Module contains main function. Run this module to run the program
###############################################################################

from sqlalchemy import (create_engine, Column, Integer, String, ForeignKey,
                        Date)
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
import datetime

# firebird database settings
firebirdDB_userName='SYSDBA'
firebirdDB_userPassword='masterke'
firebirdDB_host='localhost'
firebirdDB_name='/var/JIM.fdb'
firebirdDB_charset='win1250' # in database is 'win1250'
firebirdDB_driver='firebird+fdb'

firebirdDB_string=firebirdDB_driver+'://'+                                     \
  firebirdDB_userName+':'+firebirdDB_userPassword+'@'+                         \
  firebirdDB_host+'/'+                                     \
  firebirdDB_name+'?charset='+firebirdDB_charset

firebirdEngine=create_engine(firebirdDB_string, echo=False)
FirebirdSession=sessionmaker(bind=firebirdEngine)

# postgres database settings
postgresDB_userName='pyjim'
postgresDB_userPassword='pyjim'
postgresDB_host='localhost'
postgresDB_name='pyjim'
postgresDB_driver='postgres'

postgresDB_string=postgresDB_driver+'://'+                                     \
  postgresDB_userName+':'+postgresDB_userPassword+'@'+                         \
  postgresDB_host+'/'+postgresDB_name

postgresEngine=create_engine(postgresDB_string, echo=False)
PostgresSession=sessionmaker(bind=postgresEngine)

FirebirdBase=declarative_base()
PostgresBase=declarative_base()

#------------------------------------------------------------------------------

class FirebirdGenerationDate(FirebirdBase):
  __tablename__='data'
  id=Column(Integer, primary_key=True, name='nr_id_dec')
  date=Column(String(50), name='data')

class PostgresGenerationDate(PostgresBase):
  __tablename__='generationDate'
  id=Column(Integer, primary_key=True)
  date=Column(Date)

#------------------------------------------------------------------------------

class Gestor(PostgresBase):
  __tablename__='gestor'
  id=Column(Integer, primary_key=True)
  gestorName=Column(String(50))

#------------------------------------------------------------------------------

class Status(PostgresBase):
  __tablename__='status'
  id=Column(Integer, primary_key=True)
  status=Column(String(20))

#------------------------------------------------------------------------------

class Supply(PostgresBase):
  __tablename__='supply'
  id=Column(Integer, primary_key=True)
  supply=Column(String(50))


class MeasureUnit(PostgresBase):
  __tablename__='measureUnit'
  id=Column(Integer, primary_key=True)
  measureUnit=Column(String(30))

#------------------------------------------------------------------------------

class FirebirdJIMItem(FirebirdBase):
  __tablename__='unu_indeksy'
  id=Column(Integer, primary_key=True, name='nr_id_dec')
  JIM=Column(String(20), name='indeks')
  name=Column(String(50), name='nazwa')
  jm=Column(String(10), name='jm')
  gestor=Column(String(50), name='gestor')
  supply=Column(String(50), name='klasa_zaop')
  status=Column(String(80), name='status')
  description=Column(String, name='opis')

class PostgresJIMItem(PostgresBase):
  __tablename__='item'
  id=Column(Integer, primary_key=True)
  JIM=Column(String(20))
  name=Column(String(50))

  supplyID=Column(Integer, ForeignKey('supply.id'))
  supply=relationship('Supply', backref=backref('item'))

  gestorID=Column(Integer, ForeignKey('gestor.id'))
  gestor=relationship('Gestor', backref=backref('item'))

  statusID=Column(Integer, ForeignKey('status.id'))
  status=relationship('Status', backref=backref('item'))

  measureUnitID=Column(Integer, ForeignKey('measureUnit.id'))
  measureUnit=relationship('MeasureUnit', backref=backref('item'))

  description=Column(String)


if __name__ == '__main__':

  # creating dabases sessions
  firebirdSession=FirebirdSession()
  postgresSession=PostgresSession()

  # reseting postgres database
  PostgresBase.metadata.drop_all(postgresEngine)
  PostgresBase.metadata.create_all(postgresEngine)

  # fixing database
  # some names, descriptions having illegal bytes
  print('Pathing database: ', end='')
  # this item has illegal 0x81 byte in 100 position
#   FirebirdJIMItem.__table__.update().\
#                             where(FirebirdJIMItem.id==4806915).\
#                             values({'description': '[broken]'})
  firebirdSession.execute("UPDATE unu_indeksy SET opis='[broken]' WHERE nr_id_dec=4806915")
  # this item has illegal 0x83 byte in 206 position
#   FirebirdJIMItem.__table__.update().\
#                             where(FirebirdJIMItem.id==4818647).\
#                             values({'description': '[broken]'})
  firebirdSession.execute("UPDATE unu_indeksy SET opis='[broken]' WHERE nr_id_dec=4818647")
  print('done')

  # moving date table
  count=0
  startTime=datetime.datetime.now()
  for date in firebirdSession.query(FirebirdGenerationDate).all():
    newDate=PostgresGenerationDate()
    newDate.date=date.date
    postgresSession.add(newDate)
    count+=1
  postgresSession.commit()
  print('Moved '+str(count)+' date objects in '+str(datetime.datetime.now()-startTime))

  # collecting gestors
  count=0
  gestorDict={}
  startTime=datetime.datetime.now()
  for (gestor, ) in firebirdSession.query(FirebirdJIMItem.gestor).distinct():
    newGestor=Gestor(gestorName=gestor)
    postgresSession.add(newGestor)
    gestorDict[gestor]=newGestor
    count+=1
  postgresSession.commit()
  print('Collected '+str(count)+' gestors in '+str(datetime.datetime.now()-startTime))

  # collecting statuses
  count=0
  statusDict={}
  startTime=datetime.datetime.now()
  for (status, ) in firebirdSession.query(FirebirdJIMItem.status).distinct():
    newStatus=Status(status=status)
    postgresSession.add(newStatus)
    statusDict[status]=newStatus
    count+=1
  postgresSession.commit()
  print('Collected '+str(count)+' statuses in '+str(datetime.datetime.now()-startTime))

  # collecting supply
  count=0
  supplyDict={}
  startTime=datetime.datetime.now()
  for (supply, ) in firebirdSession.query(FirebirdJIMItem.supply).distinct():

    # fixing naming in database
    supply=' '.join(supply.split())

    newSupply=Supply(supply=supply)
    postgresSession.add(newSupply)
    supplyDict[supply]=newSupply
    count+=1
  postgresSession.commit()
  print('Collected '+str(count)+' supply classes in '+str(datetime.datetime.now()-startTime))

  # collecting measuring units
  count=0
  measureUnitDict={}
  startTime=datetime.datetime.now()
  for (measureUnit, ) in firebirdSession.query(FirebirdJIMItem.jm).distinct():

    # fixing name
    measureUnit=' '.join(measureUnit.split())

    newMeasureUnit=MeasureUnit(measureUnit=measureUnit)
    postgresSession.add(newMeasureUnit)
    measureUnitDict[measureUnit]=newMeasureUnit
    count+=1
  postgresSession.commit()
  print('Collected '+str(count)+' measure units in '+str(datetime.datetime.now()-startTime))

  # moving items
  print('Moving items:')
  count=0
  startTime=datetime.datetime.now()

  # it can be done in this way. But, we have some strange errors with coding...
  # use this, after applying all pathes
  items=[]
  for item in firebirdSession.query(FirebirdJIMItem.JIM,
                                    FirebirdJIMItem.name,
                                    FirebirdJIMItem.jm,
                                    FirebirdJIMItem.description,
                                    FirebirdJIMItem.gestor,
                                    FirebirdJIMItem.supply,
                                    FirebirdJIMItem.status).\
                                    order_by(FirebirdJIMItem.JIM).\
                                    yield_per(500): # all():
    fixedMeasureUnit=' '.join(item.jm.split())
    fixedSupply=' '.join(item.supply.split())
    # loading new JIM items into memory
    newItem={}
    newItem['JIM']=item.JIM
    newItem['name']=item.name
    newItem['measureUnitID']=measureUnitDict[fixedMeasureUnit].id
    newItem['description']=item.description
    newItem['gestorID']=gestorDict[item.gestor].id
    newItem['supplyID']=supplyDict[fixedSupply].id
    newItem['statusID']=statusDict[item.status].id
    items.append(newItem)
    count+=1

    if not count%50000:
      # every 50 000 writing changes to database and clear memory
      postgresSession.execute(PostgresJIMItem.__table__.insert(), items)
      postgresSession.commit()
      items=[]
      print('   processed '+str(count)+' items')

  postgresSession.commit()
  print('Moved '+str(count)+' item objects in '+str(datetime.datetime.now()-startTime))

  # another way... works really slowly...
  # use this to find out, which items have illegal bytes
#   items=[]
#   for (itemID, ) in firebirdSession.query(FirebirdJIMItem.id).yield_per(500):
#     try:
#       item=firebirdSession.query(FirebirdJIMItem).get(itemID)
#     except Exception as e:
#       print('  trouble with moving item id '+str(itemID)+': '+str(e))
#       continue
#     fixedMeasureUnit=' '.join(item.jm.split())
#     fixedSupply=' '.join(item.supply.split())
#     # loading new JIM item into memory
#     newItem={}
#     newItem['JIM']=item.JIM
#     newItem['name']=item.name
#     newItem['measureUnitID']=measureUnitDict[fixedMeasureUnit].id
#     newItem['description']=item.description
#     newItem['gestorID']=gestorDict[item.gestor].id
#     newItem['supplyID']=supplyDict[fixedSupply].id
#     newItem['statusID']=statusDict[item.status].id
#     items.append(newItem)
#     count+=1
#
#     if not count%50000:
#       # every 50 000 writing changes to database and clear memory
#       postgresSession.execute(PostgresJIMItem.__table__.insert(), items)
#       postgresSession.commit()
#       items=[]
#       print('   processed '+str(count)+' items')
#   postgresSession.commit()
#   print('Moved '+str(count)+' item objects in '+str(datetime.datetime.now()-startTime))

  firebirdSession.close()
  postgresSession.close()