# -*- coding: utf-8 -*-

###############################################################################
# main.py
#
# author: Paweł Surowiec (codedigger)
# creation date: 21.12.2014
# version: 0.0.1
#
# Module contains main application. Run this script to run main program.
#
# This is migration tool - for rebuild and copy data from Firebird
# to PostgreSQL database. Any settings are in selected variables (see top
# of this file).
#
# For other tools, goto main directory.
#
###############################################################################

import fdb
import psycopg2
import datetime

# setting for Firebird database
fbPath='/var/JIM.fdb'
fbUsername='SYSDBA'
fbPassword='masterkey'
fbCodepade='win1250'

# settings for PostgreSQL database
pgServer='127.0.0.1'
pgName='pyJIM'
pgUsername='pyJIM'
pgPassword='pyJIM'


if __name__ == '__main__':
  # connecting to Firebird database
  print('Connecting to '+fbPath+' Firebird database: ', end='')
  fbConnection=fdb.connect(dsn=fbPath,
                           user=fbUsername,
                           password=fbPassword,
                           charset=fbCodepade)
  fbCursor=fbConnection.cursor()
  print('done')

  # connecting to Postgres
  print('Connecting to '+pgName+' PostgreSQL database: ', end='')
  pgConnection=psycopg2.connect(host=pgServer,
                                database=pgName,
                                user=pgUsername,
                                password=pgPassword)
  pgCursor=pgConnection.cursor()
  print('done')

  print('Deleting items from PostgreSQL: ', end='')
  try:
    pgCursor.execute('DROP TABLE "item";')
  except: pgConnection.rollback()
  print('done')

  # moving measure units to Postgres
  startTime=datetime.datetime.now()
  print('Moving measure units to PostgreSQL')
  try:
    pgCursor.execute('DROP TABLE "measureUnit";')
  except: pgConnection.rollback()
  pgCursor.execute('CREATE TABLE "measureUnit" (                               \
      id serial NOT NULL,                                                      \
      "measureUnit" character varying(15) NOT NULL,                            \
      CONSTRAINT "measureUnits_pkey" PRIMARY KEY (id)                          \
    )                                                                          \
  WITH (                                                                       \
    OIDS=FALSE                                                                 \
    );')

  measureUnitDict={}
  count=1
  for (measureUnit, ) in fbCursor.execute('SELECT DISTINCT JM FROM UNU_INDEKSY;').fetchall():
    measureUnit=' '.join(measureUnit.split())
    measureUnitDict[measureUnit]=count
    count+=1
    pgCursor.execute('INSERT INTO "measureUnit" ("measureUnit") VALUES '+
                     "('"+measureUnit+"')"+
                     ';')
  pgConnection.commit()
  print('Done in '+str(datetime.datetime.now()-startTime))

  # moving supply classes to Postgres
  startTime=datetime.datetime.now()
  print('Moving supply classes to PostgreSQL')
  try:
    pgCursor.execute('DROP TABLE "supplyClass";')
  except: pgConnection.rollback()
  pgCursor.execute('CREATE TABLE "supplyClass" (                               \
      id serial NOT NULL,                                                      \
      "supplyClass" character varying(25) NOT NULL,                            \
      CONSTRAINT "supplyClass_pkey" PRIMARY KEY (id)                           \
    )                                                                          \
  WITH (                                                                       \
    OIDS=FALSE                                                                 \
    );')

  supplyClassDict={}
  count=1
  for (supplyClass, ) in fbCursor.execute('SELECT DISTINCT KLASA_ZAOP FROM UNU_INDEKSY;').fetchall():
    supplyClassDict[supplyClass]=count
    count+=1
    pgCursor.execute('INSERT INTO "supplyClass" ("supplyClass") VALUES '+
                     "('"+' '.join(supplyClass.split())+"')"
                     ';')
  pgConnection.commit()
  print('Done in '+str(datetime.datetime.now()-startTime))

  # moving gestors to Postgres
  startTime=datetime.datetime.now()
  print('Moving gestors to PostgreSQL')
  try:
    pgCursor.execute('DROP TABLE "gestor";')
  except: pgConnection.rollback()
  pgCursor.execute('CREATE TABLE "gestor" (                                    \
      id serial NOT NULL,                                                      \
      "gestor" character varying(25) NOT NULL,                                 \
      CONSTRAINT "gestor_pkey" PRIMARY KEY (id)                                \
    )                                                                          \
  WITH (                                                                       \
    OIDS=FALSE                                                                 \
    );')

  gestorDict={}
  count=1
  for (gestor, ) in fbCursor.execute('SELECT DISTINCT GESTOR FROM UNU_INDEKSY;').fetchall():
    gestorDict[gestor]=count
    count+=1
    pgCursor.execute('INSERT INTO "gestor" ("gestor") VALUES '+
                     "('"+' '.join(gestor.split())+"')"+
                     ';')
  pgConnection.commit()
  print('Done in '+str(datetime.datetime.now()-startTime))

  # moving statuses to Postgres
  startTime=datetime.datetime.now()
  print('Moving statuses to PostgreSQL')
  try:
    pgCursor.execute('DROP TABLE "status";')
  except: pgConnection.rollback()
  pgCursor.execute('CREATE TABLE "status" (                                    \
      id serial NOT NULL,                                                      \
      "status" character varying(25) NOT NULL,                                 \
      CONSTRAINT "status_pkey" PRIMARY KEY (id)                                \
    )                                                                          \
  WITH (                                                                       \
    OIDS=FALSE                                                                 \
    );')

  statusDict={}
  count=1
  for (status, ) in fbCursor.execute('SELECT DISTINCT STATUS FROM UNU_INDEKSY;').fetchall():
    statusDict[status]=count
    count+=1
    pgCursor.execute('INSERT INTO "status" ("status") VALUES '+
                     "('"+' '.join(status.split())+"')"+
                     ';')
  pgConnection.commit()
  print('Done in '+str(datetime.datetime.now()-startTime))

  # first cleaning
  # TODO: make this working - http://stackoverflow.com/questions/1017463/postgresql-how-to-run-vacuum-from-code-outside-transaction-block
#   pgCursor.execute('VACUUM')


  # moving items
  startTime=datetime.datetime.now()
  print('Moving items to PostgreSQL')
  pgCursor.execute('CREATE TABLE "item"                                        \
                    (                                                          \
                    id serial NOT NULL,                                        \
                    "JIM" character varying(20),                               \
                    "name" character varying(50),                              \
                    "supplyID" integer,                                        \
                    "gestorID" integer,                                        \
                    "statusID" integer,                                        \
                    "measureUnitID" integer,                                   \
                    "description" character varying,                           \
                    CONSTRAINT item_pkey PRIMARY KEY (id),                     \
                                                                               \
                    CONSTRAINT "item_gestorID_fkey" FOREIGN KEY ("gestorID")   \
                    REFERENCES "gestor" (id) MATCH SIMPLE                      \
                    ON UPDATE NO ACTION ON DELETE NO ACTION,                   \
                                                                               \
                    CONSTRAINT "item_measureUnitID_fkey" FOREIGN KEY ("measureUnitID")\
                    REFERENCES "measureUnit" (id) MATCH SIMPLE                 \
                    ON UPDATE NO ACTION ON DELETE NO ACTION,                   \
                                                                               \
                    CONSTRAINT "item_statusID_fkey" FOREIGN KEY ("statusID")   \
                    REFERENCES "status" (id) MATCH SIMPLE                      \
                    ON UPDATE NO ACTION ON DELETE NO ACTION,                   \
                                                                               \
                    CONSTRAINT "item_supplyID_fkey" FOREIGN KEY ("supplyID")   \
                    REFERENCES "supplyClass" (id) MATCH SIMPLE                 \
                    ON UPDATE NO ACTION ON DELETE NO ACTION                    \
                                                                               \
                    )                                                          \
                    WITH ( OIDS=FALSE );')

  # pathing database
  fbCursor.execute("UPDATE unu_indeksy SET opis='[broken]' WHERE nr_id_dec=4806915")
  fbCursor.execute("UPDATE unu_indeksy SET opis='[broken]' WHERE nr_id_dec=4818647")

  count=0
  for (jim,
       name,
       measureUnit,
       supplyClass,
       gestor,
       status,
       description ) in fbCursor.execute('SELECT "INDEKS",                     \
                                                 "NAZWA",                      \
                                                 "JM",                         \
                                                 "KLASA_ZAOP",                 \
                                                 "GESTOR",                     \
                                                 "STATUS",                     \
                                                 "OPIS" FROM UNU_INDEKSY;').iter():
    count+=1
    measureUnit=' '.join(measureUnit.split())
    pgCursor.execute('INSERT INTO "item" ("JIM", "name", "measureUnitID", "supplyID", "gestorID", "statusID", "description") VALUES '+
                     "('"+jim+"', "+
                     "'"+name.replace("'","''")+"', "+
                     ""+str(measureUnitDict[measureUnit])+", "+
                     ""+str(supplyClassDict[supplyClass])+", "+
                     ""+str(gestorDict[gestor])+", "+
                     ""+str(statusDict[status])+", "+
                     "'"+description.replace("'","''")+"')"
                     ';')
    if not count%50000: print('  moved '+str(count)+' items')
  pgConnection.commit()
  print('Done in '+str(datetime.datetime.now()-startTime))

  pgConnection.close()
  fbConnection.close()
