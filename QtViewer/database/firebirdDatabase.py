# -*- coding: utf-8 -*-

###############################################################################
# firebirdDatabase.py
#
# author: Paweł Surowiec (codedigger)
# creation date: 28.11.2014
# version: 0.0.1
#
# Module contains all functions to cooperate with database
###############################################################################

firebirdDatabase_py_VERSION=(0,0,1)

from sqlalchemy import create_engine, Column, Integer, String, Boolean, ForeignKey, DateTime
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

# database settings
firebirdDB_userName='SYSDBA'
firebirdDB_userPassword='masterke'
firebirdDB_host='localhost'
firebirdDB_name='/var/JIM.fdb'
firebirdDB_charset='win1250'
firebirdDB_driver='firebird+fdb'

firebirdDB_string=firebirdDB_driver+'://'+                                     \
  firebirdDB_userName+':'+firebirdDB_userPassword+'@'+                         \
  firebirdDB_host+'/'+                                     \
  firebirdDB_name+'?charset='+firebirdDB_charset

firebirdEngine=create_engine(firebirdDB_string,
                             echo=False )
firebirdSession=sessionmaker(bind=firebirdEngine)

Base=declarative_base()

class GenerationDate(Base):
  __tablename__='data'

  id=Column(Integer,
            primary_key=True,
            name='nr_id_dec')
  date=Column(String(50),
              name='data')

class Gestor(Base):
  __tablename__='gestor'

  id=Column(Integer,
            primary_key=True,
            name='nr_id_dec')
  gestorName=Column(String(50),
                    name='gestor')

class Status(Base):
  __tablename__='status'

  id=Column(Integer,
            primary_key=True,
            name='nr_id_dec')
  status=Column(String(50),
                name='status')

class SupplyClass(Base):
  __tablename__='klasa_zaop'

  id=Column(Integer,
            primary_key=True,
            name='nr_id_dec')
  supplyClass=Column(String(50),
                     name='klasa_zaop')

class JIMItem(Base):
  __tablename__='unu_indeksy'

  id=Column(Integer,
            primary_key=True,
            name='nr_id_dec')

  JIM=Column(String(20),
               name='indeks')

  name=Column(String(50),
              name='nazwa')

  jm=Column(String(10),
            name='jm')

  supplyClassID=Column(Integer,
                       ForeignKey('klasa_zaop.nr_id_dec'),
                       name='klasa_zaop')
  supplyClass=relationship('SupplyClass', backref=backref('unu_indeksy'))

  gestorID=Column(Integer,
                  ForeignKey('gestor.nr_id_dec'),
                  name='gestor')
  gestor=relationship('Gestor', backref=backref('unu_indeksy'))

  statusID=Column(Integer,
                  ForeignKey('status.nr_id_dec'),
                  name='status')
  status=relationship('Status', backref=backref('unu_indeksy'))

  description=Column(String,
                       name='opis')

