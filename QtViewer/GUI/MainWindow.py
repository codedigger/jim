# -*- coding: utf-8 -*-

###############################################################################
# MainWindow.py
#
# author: Paweł Surowiec (codedigger)
# creation date: 26.11.2014
# version: 0.0.1
#
# Module contains main window with all widgets.
###############################################################################

MainWindow_py_VERSION=(0,0,1)

from PySide import QtGui, QtCore

from QtD.MainWindow import Ui_MainWindow
from database import firebirdDatabase

class MainWindow(Ui_MainWindow, QtGui.QMainWindow):
  """Main window class"""

  # firebird database session
  fbSession=None

  def __init__(self):
    """Default class initializer"""
    super(MainWindow, self).__init__()
    self.setupUi(self)

    # getting database date
    self.fbSession=firebirdDatabase.firebirdSession()
    creationDate=self.fbSession.query(firebirdDatabase.GenerationDate).\
                           filter(firebirdDatabase.GenerationDate.id==1).\
                           one()
    self.txtDescription.setText('Baza danych z dnia '+creationDate.date)

    # getting gestors
    gestors=self.fbSession.query(firebirdDatabase.Gestor.gestorName).all()
    self.cmbGestor.addItem('-- Wszyscy --')
    for gestor in gestors: self.cmbGestor.addItem(gestor.gestorName)

    # getting statuses
    statuses=self.fbSession.query(firebirdDatabase.Status.status).all()
    self.cmbStatus.addItem('-- Wszystkie --')
    for status in statuses: self.cmbStatus.addItem(status.status)

    # getting supply classes
    supplyClasses=self.fbSession.query(firebirdDatabase.SupplyClass.supplyClass).all()
    self.cmbSupplyClass.addItem('-- Wszystkie --')
    for supplyClass in supplyClasses: self.cmbSupplyClass.addItem(supplyClass.supplyClass)

    self.btnSearch.clicked.connect(self.searchDatabase)

    self.listItems.currentItemChanged.connect(self.listItemClicked)

    # after pressing ENTER (or RETURN in Qt naming), trigger click() event
    # on btnSearch
    self.txtSearch.keyPressEvent=lambda x: self.btnSearch.click() if x.key()==QtCore.Qt.Key_Return else QtGui.QLineEdit.keyPressEvent(self.txtSearch, x)

  def searchDatabase(self):
    self.listItems.clear()

    searchStr=self.txtSearch.text().upper()
    query=self.fbSession.query(firebirdDatabase.JIMItem)

    if   self.cmbSearchFilter.currentIndex()==0: searchItem=firebirdDatabase.JIMItem.name
    elif self.cmbSearchFilter.currentIndex()==1: searchItem=firebirdDatabase.JIMItem.description
    elif self.cmbSearchFilter.currentIndex()==2: searchItem=firebirdDatabase.JIMItem.JIM

    if self.rbtSearchExactly.isChecked():
      query=query.filter(searchItem==searchStr)
    elif self.rbtSearchEvery.isChecked():
      searchString=searchStr.replace(' ','%')
      query=query.filter(searchItem.like('%'+searchString+'%'))

    items=query.all()

    for item in items:
      jimItem=QtGui.QListWidgetItem()
      jimItem.setText(item.name)
      jimItem.setData(QtCore.Qt.UserRole, item.id)
      self.listItems.addItem(jimItem)

  def listItemClicked(self):
    currItemId=self.listItems.currentItem().data(QtCore.Qt.UserRole)
    item=self.fbSession.query(firebirdDatabase.JIMItem).\
                        filter(firebirdDatabase.JIMItem.id==currItemId).\
                        one()
    itemText=''

    itemText+='<b>Nazwa sprzętu:</b><br />'
    itemText+=item.name+'<hr />'

    itemText+='<b>Numer JIM:</b><br />'
    itemText+=item.JIM+'<hr />'

    itemText+='<b>Jednostka miary:</b><br />'
    itemText+=item.jm+'<hr />'

    itemText+='<b>Opis:</b><br />'
    itemText+=item.description+'<hr />'

#     itemText+='<b>Status:</b><br />'
#     itemText+=item.status.status+'<hr />'

    self.txtDescription.setText(itemText)