# -*- coding: utf-8 -*-

###############################################################################
# MainWindow.py
#
# author: Paweł Surowiec (codedigger)
# creation date: 26.11.2014
# version: 0.0.1
#
# Module contains main function. Run this module to run the program
###############################################################################

main_py_VERSION=(0,0,1)

from PySide import QtGui
from PySide.QtCore import qDebug

from GUI.MainWindow import MainWindow, MainWindow_py_VERSION
from database import firebirdDatabase


if __name__ == '__main__':
  import sys

  qDebug('Starting main application')
  qDebug('   > main module version: '+str(main_py_VERSION))
  qDebug('   > MainWindow module version: '+str(MainWindow_py_VERSION))
  qDebug('   > firebirdDatabase module version: '+str(firebirdDatabase.firebirdDatabase_py_VERSION))

  # creating base windows
  app=QtGui.QApplication(sys.argv)
  mainWindow=MainWindow()
  mainWindow.show()

  # main loop
  sys.exit(app.exec_())