# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.views.generic import ListView, TemplateView

from .models import Gestor

class GestorListView(ListView):
  model=Gestor
  template_name='gestorsList.html'

gestorsListView=GestorListView.as_view()

class HomePageView(TemplateView):
  template_name='base.html'

homePageView=HomePageView.as_view()