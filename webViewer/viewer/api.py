# -*- coding: utf-8 -*-

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Gestor, Item, MeasureUnit, Status, Supply
from .serializers import (DefGestorSerializer, DefItemSerializer, ItemSerializer, MinItemSerializer,
                          DefMeasureUnitSerializer, DefStatusSerializer,
                          DefSupplySerializer)


class GestorList(generics.ListAPIView):
  queryset=Gestor.objects.all()
  serializer_class=DefGestorSerializer

class MeasureUnitList(generics.ListAPIView):
  queryset=MeasureUnit.objects.all()
  serializer_class=DefMeasureUnitSerializer

class StatusList(generics.ListAPIView):
  queryset=Status.objects.all()
  serializer_class=DefStatusSerializer

class SupplyList(generics.ListAPIView):
  queryset=Supply.objects.all()
  serializer_class=DefSupplySerializer

class ItemList(generics.ListAPIView):
  serializer_class=MinItemSerializer

  def get_queryset(self):
    searchString = self.request.QUERY_PARAMS.get('for', '')
    searchType=self.request.QUERY_PARAMS.get('searchType', 'All')
    searchField=self.request.QUERY_PARAMS.get('searchField', 'Name')

    statusID=self.request.QUERY_PARAMS.get('status', None)
    supplyID=self.request.QUERY_PARAMS.get('supply', None)
    measureUnitID=self.request.QUERY_PARAMS.get('measureUnit', None)
    gestorID=self.request.QUERY_PARAMS.get('gestor', None)

    # Looks little strange, but we don't know, what will be in request
    if searchField=='Name': field='name'
    elif searchField=='JIM': field='JIM'
    elif searchField=='Description': field='description'

    if searchString=='':
      queryset=Item.objects.all().order_by('id')
    else:
      if searchType=='Exactly':
        queryset=Item.objects.filter( **{field: searchString} )
      elif searchType=='All':
        queryset=Item.objects.all()
        for word in searchString.split():
          queryset&=Item.objects.filter( **{field+'__icontains': word} )
      else:
        queryset=Item.objects.none()
        for word in searchString.split():
          queryset|=Item.objects.filter( **{field+'__icontains': word} )

    if statusID: queryset=queryset.filter(status__id=statusID)
    if supplyID: queryset=queryset.filter(supply__id=supplyID)
    if measureUnitID: queryset=queryset.filter(measureUnit__id=measureUnitID)
    if gestorID: queryset=queryset.filter(gestor__id=gestorID)

    if queryset.count() > 10000: self.paginate_by=10

    return queryset

class ItemDetail(generics.RetrieveAPIView):
  queryset=Item.objects.all()
  serializer_class=DefItemSerializer


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'gestors': reverse('apiGestorList', request=request, format=format),
        'measureUnits': reverse('apiMeasureUnitList', request=request, format=format),
        'statuses': reverse('apiStatusList', request=request, format=format),
        'supplies': reverse('apiSupplyList', request=request, format=format),
        'items': reverse('apiItemList', request=request, format=format),
    })