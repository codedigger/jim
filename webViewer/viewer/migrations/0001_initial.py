# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Generationdate',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('date', models.DateField(null=True, blank=True)),
            ],
            options={
                'db_table': 'generationDate',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Gestor',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('gestorName', models.CharField(max_length=50, blank=True)),
            ],
            options={
                'db_table': 'gestor',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('JIM', models.CharField(max_length=20, blank=True)),
                ('name', models.CharField(max_length=50, blank=True)),
            ],
            options={
                'db_table': 'item',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Measureunit',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('measureUnit', models.CharField(max_length=30, blank=True)),
            ],
            options={
                'db_table': 'measureUnit',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('status', models.CharField(max_length=20, blank=True)),
            ],
            options={
                'db_table': 'status',
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Supply',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('supply', models.CharField(max_length=50, blank=True)),
            ],
            options={
                'db_table': 'supply',
                'managed': False,
            },
            bases=(models.Model,),
        ),
    ]
