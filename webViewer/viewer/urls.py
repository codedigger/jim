from django.conf.urls import patterns, include, url

from .api import (GestorList, MeasureUnitList, StatusList, SupplyList, ItemList,
                  ItemDetail, api_root)

gestor_urls = patterns('',
    url(r'^$', GestorList.as_view(), name='apiGestorList')
)

measureUnit_urls = patterns('',
    url(r'^$', MeasureUnitList.as_view(), name='apiMeasureUnitList')
)

status_urls = patterns('',
#     url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', StatusList.as_view(), name='apiStatusList')
)

supply_urls = patterns('',
#     url(r'^/(?P<pk>\d+)$', PhotoDetail.as_view(), name='photo-detail'),
    url(r'^$', SupplyList.as_view(), name='apiSupplyList')
)

item_urls = patterns('',
    url(r'^/(?P<pk>\d+)$', ItemDetail.as_view(), name='itemDetail'),
    url(r'^$', ItemList.as_view(), name='apiItemList')
)

api_urls = patterns('',
    url(r'^gestors', include(gestor_urls)),
    url(r'^measureUnits', include(measureUnit_urls)),
    url(r'^statuses', include(status_urls)),
    url(r'^supplies', include(supply_urls)),
    url(r'^items', include(item_urls)),
    url(r'^$', api_root),
)


urlpatterns = patterns('',
    url(r'^api/', include(api_urls)),
    url(r'^gestors/$', 'viewer.views.gestorsListView', name='gestorsListView'),
    url(r'^$', 'viewer.views.homePageView', name='homePage'),
)