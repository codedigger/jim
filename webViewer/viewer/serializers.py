# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import Gestor, Item, MeasureUnit, Status, Supply

class DefGestorSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model=Gestor
    fields=('id', 'gestorName')

class DefMeasureUnitSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model=MeasureUnit
    fields=('id', 'measureUnit')

class DefStatusSerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model=Status
    fields=('id', 'status')

class DefSupplySerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model=Supply
    fields=('id', 'supply')

class DefItemSerializer(serializers.HyperlinkedModelSerializer):
  gestor=DefGestorSerializer()
  measureUnit=DefMeasureUnitSerializer()
  status=DefStatusSerializer()
  supply=DefSupplySerializer()
  class Meta:
    model=Item
    fields=('id', 'name', 'gestor', 'JIM', 'measureUnit', 'status', 'supply', 'description')

class ItemSerializer(serializers.ModelSerializer):
  """ItemSerializer

  Modified default serializer:
  - gestor, measureUnit, status and supply are in string representation
  """
  gestor=serializers.StringRelatedField()
  measureUnit=serializers.StringRelatedField()
  status=serializers.StringRelatedField()
  supply=serializers.StringRelatedField()
  class Meta:
    model=Item
    fields=('id', 'name', 'gestor', 'JIM', 'measureUnit', 'status', 'supply', 'description')

class MinItemSerializer(serializers.ModelSerializer):
  """MinItemSerializer

  Modified default serializer:
  - serialize only three fields
  """
  class Meta:
    model=Item
    fields=('id', 'JIM', 'name')