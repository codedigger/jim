# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models


class GenerationDate(models.Model):
    id = models.IntegerField(primary_key=True)
    date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'generationDate'

    def __str__(self):
      return str(self.date)


class Gestor(models.Model):
    id = models.IntegerField(primary_key=True)
    gestorName = models.CharField(max_length=50,
                                  blank=True)

    class Meta:
        managed = False
        db_table = 'gestor'

    def __str__(self):
      return '%s: %s' % (self.id, self.gestorName)


class MeasureUnit(models.Model):
    id = models.IntegerField(primary_key=True)
    measureUnit = models.CharField(max_length=30,
                                   blank=True)

    class Meta:
        managed = False
        db_table = 'measureUnit'

    def __str__(self):
      return '%s: %s' % (self.id, self.measureUnit)


class Status(models.Model):
    id = models.IntegerField(primary_key=True)
    status = models.CharField(max_length=20,
                              blank=True)

    class Meta:
        managed = False
        db_table = 'status'

    def __str__(self):
      return '%s: %s' % (self.id, self.status)


class Supply(models.Model):
    id = models.IntegerField(primary_key=True)
    supply = models.CharField(max_length=50,
                              blank=True)

    class Meta:
        managed = False
        db_table = 'supply'

    def __str__(self):
      return '%s: %s' % (self.id, self.supply)

class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    JIM = models.CharField(max_length=20,
                           blank=True)
    description=models.TextField()
    name = models.CharField(max_length=50, blank=True)

    supply = models.ForeignKey('Supply',
                               db_column='supplyID',
                               blank=True,
                               null=True)
    gestor = models.ForeignKey(Gestor,
                               db_column='gestorID',
                               blank=True,
                               null=True)
    status = models.ForeignKey('Status',
                               db_column='statusID',
                               blank=True,
                               null=True)
    measureUnit = models.ForeignKey('Measureunit',
                                    db_column='measureUnitID',
                                    blank=True,
                                    null=True)

    class Meta:
        managed = False
        db_table = 'item'

    def __str__(self):
      return '%s: %s, %s' % (self.id, self.JIM, self.name)