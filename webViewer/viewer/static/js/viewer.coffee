app = angular.module 'viewer.mainApp', ['ui.bootstrap']

app.controller 'viewerApp', ($scope, $http) ->

    class Filter
    #
    # Default filter class
    #
        constructor: (@getURL) ->
            @filterActive = false
            @selectedValue = {}
            @values = []

        getFromServer: =>
            # get data from server based on getURL
            $http.get(@getURL).then \
                (result) =>
                    angular.forEach result.data, (item) =>
                        @values.push(item)
                ,
                (result) =>
                    alert('Problem with getting data from '+@getURL)



    class ItemShelf
    #
    # Shelf with items details
    #
        constructor: (@getItemURL) ->
            @items = []

        addItem: (item) =>
            itemExist=false

            # searching for item
            for i in @items
                if i.id==item.id
                    itemExist=true
                    break

            # if item does not exist, add it to shelf
            if not itemExist
                $http.get(@getItemURL+item.id).then \
                    (result) =>
                        @items.push(result.data)
                    ,
                    (result) =>
                        alert('Problem with getting item detail from '+@getURL)

        deleteItem: (item) =>
            # deleting item from shelf
            @items.splice(@items.indexOf(item), 1)


    class Items
    #
    # class representation of item list from database
    #
        constructor: (@getURL) ->
            @values = []              # all items from server
            @disValues = []           # currently displayed items
            @isPaginated = false      # data from server are paginated?
            @currPage = 1             # current display page
            @itemsNumber = 0          # items in values
            @filter = ''              # what we are looking for
            @searchField = 'Name'     # in what field
            @searchType = 'All'       # and what words

        newSearch: =>
            #
            # function begins new search
            #
            @currPage=1
            @update()

        changePage: =>
            #
            # function change displayed items
            #

            # if we have paginated data, retrive new page from server
            if @isPaginated
                @update()

            # else change displayed items
            else
                startItem=(@currPage-1)*10
                endItem=Math.min(startItem+10, @values.length)
                @disValues=@values[startItem..endItem]

        update: =>
            #
            # function retrives new data from server
            #

            # clearing old item list
            @values=[]
            @disValues=[]

            # building search string
            searchString=@getURL+
                         'page='+@currPage+'&'+
                         'for='+@filter+'&'+
                         'searchType='+@searchType+'&'+
                         'searchField='+@searchField+'&'

            searchString+='status='+$scope.statuses.selectedValue.id+'&' if $scope.statuses.filterActive
            searchString+='supply='+$scope.supplies.selectedValue.id+'&' if $scope.supplies.filterActive
            searchString+='measureUnit='+$scope.measureUnits.selectedValue.id+'&' if $scope.measureUnits.filterActive
            searchString+='gestor='+$scope.gestors.selectedValue.id+'&' if $scope.gestors.filterActive

#             sending request to server
            $http.get(searchString).then \
                (result) =>
                    # if we get a lot of results, data are paginated (> 10 000 items)
                    if result.data.count
                        angular.forEach result.data.results, (item) =>
                            @values.push(item)
                        @itemsNumber=result.data.count
                        @disValues=@values
                        @isPaginated=true
                    # we have all data in one response
                    else
                        angular.forEach result.data, (item) =>
                            @values.push(item)
                        @itemsNumber=@values.length
                        @disValues=@values[0..9]
                        @isPaginated=false
                ,
                (result) ->
                    alert('Problem with searching item\nSearch string: '+searchString)

#-------------------------------------------------------------------------------

    $scope.filterPanelOpened=false

    #creating filters
    $scope.statuses = new Filter('/api/statuses')
    $scope.supplies = new Filter('/api/supplies')
    $scope.measureUnits = new Filter('/api/measureUnits')
    $scope.gestors = new Filter('/api/gestors')

    # creating item shelf
    $scope.itemShelf = new ItemShelf('/api/items/')

    # getting information from database
    $scope.statuses.getFromServer()
    $scope.supplies.getFromServer()
    $scope.measureUnits.getFromServer()
    $scope.gestors.getFromServer()

    # creating item list
    $scope.items = new Items('/api/items?')
    $scope.items.update()
