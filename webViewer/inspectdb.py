# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Generationdate(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'generationDate'


class Gestor(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    gestorname = models.CharField(db_column='gestorName', max_length=50, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'gestor'


class Item(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    jim = models.CharField(db_column='JIM', max_length=20, blank=True)  # Field name made lowercase.
    name = models.CharField(max_length=50, blank=True)
    supplyid = models.ForeignKey('Supply', db_column='supplyID', blank=True, null=True)  # Field name made lowercase.
    gestorid = models.ForeignKey(Gestor, db_column='gestorID', blank=True, null=True)  # Field name made lowercase.
    statusid = models.ForeignKey('Status', db_column='statusID', blank=True, null=True)  # Field name made lowercase.
    measureunitid = models.ForeignKey('Measureunit', db_column='measureUnitID', blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(max_length=-1, blank=True)

    class Meta:
        managed = False
        db_table = 'item'


class Measureunit(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    measureunit = models.CharField(db_column='measureUnit', max_length=30, blank=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'measureUnit'


class Status(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    status = models.CharField(max_length=20, blank=True)

    class Meta:
        managed = False
        db_table = 'status'


class Supply(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    supply = models.CharField(max_length=50, blank=True)

    class Meta:
        managed = False
        db_table = 'supply'
